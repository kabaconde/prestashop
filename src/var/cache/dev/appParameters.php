<?php return array (
  'parameters' => 
  array (
    'database_host' => 'mariadb',
    'database_port' => '',
    'database_name' => 'prestashop',
    'database_user' => 'root',
    'database_password' => 'root',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'i09qp1Szuqcihm3J4dSp8qOLyaG00grXefczbury5CHNF4nWpY4rnbUa',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-11-29',
    'locale' => 'fr-FR',
    'cookie_key' => 'uNoYU4CSFzUSr5yK368Jr8UrW4CHpHOVz8twa0h8940g0SsAoqs7AxRZ',
    'cookie_iv' => 'dXrAebbb',
    'new_cookie_key' => 'def00000666262cc884b68b4bcc5ce98b3732ea804432460919dcc6f6299d12f45577685c026c21ca97bde572110cca9a3572ec20e14470c3bc937e0c319ff3b7d51e969',
  ),
);
